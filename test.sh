#!/bin/sh

# Make it TAP compliant, see http://testanything.org/tap-specification.html
echo "1..3"

failed=0
step=1

# Project found, artifact generated
desc="Generate expected artifact"
docker run --rm \
	--volume ${CI_PROJECT_DIR}/test/fixtures:/tmp/project \
	--env CI_PROJECT_DIR=/tmp/project \
	$IMAGE_TAG /analyzer run

got="${CI_PROJECT_DIR}/test/fixtures/gl-sast-report.json"
expect="${CI_PROJECT_DIR}/test/expect/gl-sast-report.json"

if test $? -eq 0 && diff $got $expect; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))

# Project not found
desc="Exit with exit status 3 when project not found"
docker run --rm \
	--volume ${CI_PROJECT_DIR}/test/fixtures/empty:/tmp/project \
	--env CI_PROJECT_DIR=/tmp/project \
	$IMAGE_TAG /analyzer run

if [ $? -eq 3 ]; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))

# Failure
desc="Exit with exit status 1 when there's something wrong"
docker run --rm \
	--volume ${CI_PROJECT_DIR}/test/fixtures/broken:/tmp/project \
	--env CI_PROJECT_DIR=/tmp/project \
	$IMAGE_TAG /analyzer run

if [ $? -eq 1 ]; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))

# Finish tests
count=$((step-1))
if [ $failed -ne 0 ]; then
  echo "Failed $failed/$count tests"
  exit 1
else
  echo "Passed $count tests"
fi
