FROM maven:3.6-jdk-8

ARG FSB_VERSION
ENV FSB_VERSION ${FSB_VERSION:-1.8.0}

RUN apt-get update && \
    apt-get install -y \
      bash curl git openssh-client

# Install Find Security Bugs
ADD https://github.com/find-sec-bugs/find-sec-bugs/releases/download/version-${FSB_VERSION}/findsecbugs-cli-${FSB_VERSION}.zip /tmp/findsecbugs-cli-${FSB_VERSION}.zip
RUN mkdir findsecbugs-cli
RUN unzip -d /findsecbugs-cli /tmp/findsecbugs-cli-${FSB_VERSION}.zip
RUN mv /findsecbugs-cli/lib/findsecbugs-plugin-${FSB_VERSION}.jar /findsecbugs-cli/lib/findsecbugs-plugin.jar
RUN rm /tmp/findsecbugs-cli-${FSB_VERSION}.zip

COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
